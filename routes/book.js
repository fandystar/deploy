const express = require("express");
const router = express.Router();

const bookControllers = require("../controllers/book");
const multer = require("../middlewares/multer");

router.post("/add", multer.single("bookImage"), bookControllers.Create);
router.get("/", bookControllers.Read);
router.put("/edit/:id", multer.single("bookImage"), bookControllers.Update);
router.delete("/delete/:id", bookControllers.Delete);

module.exports = router;